<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\VideoMetadata;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(VideoMetadata::class, function (Faker $faker) {
    return [
        'size' => $faker->numberBetween(50, 1000),
        'viewers_count' => $faker->numberBetween(1000, 100000)
    ];
});