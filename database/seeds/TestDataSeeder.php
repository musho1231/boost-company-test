<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 10)->create()->each(function ($user) {
            factory(App\Models\Video::class, 5)->create(['user_id' => $user->id])->each(function($video)use($user){
                factory(App\Models\VideoMetadata::class, 3)->create(['video_id' => $video->id]);
            });
        });
    }
}
