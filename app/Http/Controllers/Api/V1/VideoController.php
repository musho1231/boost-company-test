<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Video\Metadata\UpdateRequest;
use App\Http\Resources\Video\MetadataResource;
use App\Models\Video;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    /**
     * @param Video $video
     * @return MetadataResource
     */
    public function getMetadata(Video $video)
    {
        return new MetadataResource($video->metadata);
    }

    /**
     * @param UpdateRequest $request
     * @param Video $video
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMetadata(UpdateRequest $request, Video $video)
    {
        $video->metadata()->update($request->validated());

        return response()->json(['success' => true]);
    }
}
