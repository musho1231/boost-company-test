<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\User\TotalVideoSizeResource;
use App\Models\User;
use App\Models\Video;
use App\Models\VideoMetadata;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * @param $username
     * @return TotalVideoSizeResource
     */
    public function getTotalVideoSize($username)
    {
        // If user exist on our system, otherwise show 404 error
        $user = User::where('username', $username)->firstOrFail();
        $size = DB::table('videos')
            ->join('video_metadata', 'videos.id', '=', 'video_metadata.video_id')
            ->where('user_id', $user->id)
            ->select(DB::raw('SUM(size) as total_size'))->first();

        // This is the alternative query with laravel, which is too slowly
        // $size = VideoMetadata::whereIn('video_id',  $user->videos()->pluck('id'))->sum('size');

        if (!$size->total_size) {
            $response = response()->json(['success' => false, 'message' => 'User do not have any videos!'],404);
        }else{
            $response = new TotalVideoSizeResource($size);
        }

        return $response;
    }
}
