<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoMetadata extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function video()
    {
        return $this->belongsTo(Video::class);
    }
}
