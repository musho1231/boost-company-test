<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api\V1')->group(function () {
    Route::get('/user/{username}/total-video-size', 'UserController@getTotalVideoSize');
    Route::get('/video/{video}/metadata', 'VideoController@getMetadata');
    Route::patch('/video/{video}/metadata', 'VideoController@updateMetadata');
});